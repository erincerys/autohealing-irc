# Overrides
#node.default[:environment_name]	= [primary|secondary]
#node.default[:s3_bucket]					= [irc-ncal|irc-oregon]
#node.default[:s3_region]					= [us-west-1|us-west-2]
#node.default[:ircd_version]			= '2.0.24'
#node.default[:services_version]	= '2.0.2'
#node.default[:elastic_ip]				= '52.8.199.178'
#node.default[:route53_hosted_zone]	= 'Z1BVV607NNAR0I'
#node.default[:route53_record_name]	= 'irc-test.pound.chat.''
#node.default[:sns_notification_arn] = 'arn:aws:sns:us-west-1:183912708525:Ian'
#node.default[:primary_fqdn] = 'irc.pound.chat'

# Static
node.default[:ircd_name]                = 'inspircd'
node.default[:services_name]            = 'anope'
node.default[:chef_temp_path]           = '/tmp/chef'
node.default[:aws_region]					      = #{node['ec2']['placement_availability_zone']}[0...-1]
node.default[:letsencrypt_cert_path]    = "/etc/letsencrypt/live/#{{node[:primary_fqdn]}}/"
