#!/bin/python

# intrinsic functions and objects
from troposphere import GetAtt, Join
from troposphere import Parameter, Output, Ref, Template, Tags
from troposphere import cloudformation

# service handling
from troposphere import ec2
from troposphere import autoscaling
from troposphere import iam
from troposphere import sns

# begin template
t = Template()

t.add_description("Creates infrastructure to support a resilient irc server")

# parameters

KeyName = t.add_parameter(Parameter(
	"KeyName",
	Type="String",
	Description="Name of EC2 keypair for SSH to instance"
))

# api calls

## get amazon linux ami id

# resources



