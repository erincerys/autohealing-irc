#!/bin/bash

# Usage: bash $0 'template-name'

TODAY=$(date "+%Y%m%s")
CFN_TEMPLATE="$PWD/$1"
CFN_TEMPLATE_PREFIX=$1

CFN_DSL_PKG=`gem list --local cloudformation-ruby-dsl | tail -n1`
if [ ! "$CFN_DSL_PKG" ] ; then
	echo "You need to 'bundle install'."
	exit 1
fi

## Find where the aws cli binary is, and exit if we can't find it
AWSCLI_BINARY=$(which aws)
if [ ! "$AWSCLI_BINARY" ] ; then
	echo "You need the AWS CLI to run this."
	exit 1
fi

echo 'Expanding Cloudformation Ruby DSL template into JSON...'
./$CFN_TEMPLATE_PREFIX.rb expand > $CFN_TEMPLATE_PREFIX-$TODAY.json

## Logic to exit in case the development provided invalidation list doesn't exist or is empty
while [ ! -e "$CFN_TEMPLATE" ] ; do
	echo 'Cloudformation Ruby DSL template file not found at $(dirname "$CFN_TEMPLATE")).'
	read -p 'Provide the full path to its location: ' CFN_TEMPLATE
done
if [ ! -s "$CFN_TEMPLATE" ] ; then
	echo 'Cloudformation Ruby DSL template was empty. Exiting.'
	exit 0
elif [ ! "$(file --mime "$CFN_TEMPLATE" | grep -o 'text/plain')" ] ; then
	echo 'Cloudformation Ruby DSL template was not the expected mimetype (text/plain)'
	exit 1
fi

## Clean up environment variables in case they are wonky
if [ "${AWS_ACCESS_KEY_ID-unset}" == '' ] ; then unset AWS_ACCESS_KEY_ID ; export AWS_ACCESS_KEY_ID ; fi
if [ "${AWS_SECRET_ACCESS_KEY-unset}" == '' ] ; then unset AWS_SECRET_ACCESS_KEY ; export AWS_SECRET_ACCESS_KEY ; fi
if [ "${AWS_DEFAULT_PROFILE-unset}" == '' ] ; then unset AWS_DEFAULT_PROFILE ; export AWS_DEFAULT_PROFILE ; fi
if [ "${AWS_DEFAULT_REGION-unset}" == '' ] ; then unset AWS_DEFAULT_REGION ; export AWS_DEFAULT_REGION ; fi

## Determine where to source AWS credentials from - environment variables or config file profiles
while [ ! "$(echo "$selection" | grep -P '(y|Y)')" ] ; do
	echo "You have the following credentials configured for the AWS CLI."
	if [[ "${AWS_ACCESS_KEY_ID-unset}" != 'unset' && "${AWS_SECRET_ACCESS_KEY-unset}" != 'unset' ]] ; then
		echo "- Crdential source: Environment variables"
		echo "- Access Key: $AWS_ACCESS_KEY_ID"
		echo "- Secret Key: $AWS_SECRET_ACCESS_KEY"
	else
		if [ "${AWS_DEFAULT_PROFILE-unset}" != 'unset' ] ; then
			echo "- Credential source: $AWS_DEFAULT_PROFILE profile"
		else
			echo "- Credential source: default profile"
		fi
		echo "- Access Key: $("$AWSCLI_BINARY" configure get aws_access_key_id)"
		echo "- Secret Key: $("$AWSCLI_BINARY" configure get aws_secret_access_key)"
	fi
	if [ "$AWS_DEFAULT_REGION" ] ; then
		echo "- Region: $AWS_DEFAULT_REGION"
	else
		echo "- Region: $("$AWSCLI_BINARY" configure get region)"
	fi
	read -p 'Do you wish to use these, or something else (y/profile-name/n)? ' selection
	if [ ! "$(echo "$selection" | grep -P '(n|N|y|Y)')" ] ; then
		export AWS_DEFAULT_PROFILE=$selection
	elif [ "$(echo "$selection" | grep -P '(n|N)')" ] ; then
		echo 'Aborting. Please reconfigure AWS how you like it (e.g. `aws configure`)'
		exit 0
	fi
done

$AWSCLI_BINARY cloudformation create-stack --stack-name $CFN_STACK_NAME --disable-rollback --template-body file:///$PWD/$CFN_TEMPLATE_PREFIX-$TODAY.json --capabilities CAPABILITY_IAM --cli-input-json file:///$PWD/$CFN_TEMPLATE_PREFIX-params.json
