#!/usr/bin/env ruby

# Usage:
#   ruby $0 expand -Environment {ENV_NAME} > network.json
#   ${EDITOR} parameters.json
#   aws --profile {PROFILE} cloudformation create-stack --template-body file://network.json --cli-input-json file://parameters.json  --tags file://tags.json

# Sets up the network that the application stack(s) will leverage

require 'bundler/setup'
require 'cloudformation-ruby-dsl/cfntemplate'
require 'cloudformation-ruby-dsl/spotprice'
require 'cloudformation-ruby-dsl/table'

# To change the prefix of most resources, interpret the -Environment switch passed to ARGV at Ruby DSL expansion of this CFN template
$APP_NAME = "Irc"
(0..(ARGV.length - 1)).each do |argIndex|
  if ARGV[argIndex] =~ /^-/ then
    case ARGV[argIndex]
    when "-Environment"
      # override the default since value passed in for 'Environment'
      $APP_NAME = "Irc" + ARGV[argIndex + 1].to_s
      $ENVIRONMENT = ARGV[argIndex + 1].to_s
    end
  end
end

# Lets replace the resource DSL method with our own enhanced version
# TODO - replace this monkeypatch with a post process method on the output of template
# that simply adds missing Name tags based on the resource name
class TemplateDSL < JsonObjectDSL
  # Note: we could use alias if we wanted to retain access to the old definition
  def resource(name, options)
    # not all resources support Tags... so unfortunately, we cant automatically create the Tags hash... it has to be manually specified
    if options[:Properties] and options[:Properties][:Tags]
      unless (options[:Properties][:Tags].select {|i| i['Value'] == 'Name'}).any?
        # by convention, add the resource name tag to all resources by adding spaces to the CamelCase resource name specified
        options[:Properties][:Tags] << { Key: 'Name', Value: $APP_NAME + ' ' + name.gsub(/([a-z,0-9])([A-Z,0-9])/, '\1 \2').strip }
      end
    end

    default(:Resources, {})[name] = options
  end
end

# DSL specification of the Cloudformation JSON  ---------
template do

  value AWSTemplateFormatVersion: '2010-09-09'

  value Description: 'Creates the dependent resources necessary to support application stacks'

  value :Metadata => {
    :Author => 'Ian Schoonover',
    :'AWS::CloudFormation::Interface' => {
      :ParameterGroups => [
        {
          :Label => { :default => 'Common Network Parameters' },
          :Parameters => [
            'VpcId',
            'AvailabilityZoneLetters',
            'PublicRouteTableId',
            # 'PrivateRouteTableIds',
            'PublicSubnetCidrBlocks',
            # 'PrivateSubnetCidrBlocks',
          ],
        },
        {
          :Label => { :default => 'Template Configuration Parameters' },
          :Parameters => [
            'UseThreeAzs',
          ],
        },
      ],
    },
  }

  parameter 'VpcId',
    Type: "String",
    Description: "The ID of an existing virtual private cloud to deploy our application into",
    MinLength: "1",
    MaxLength: "12",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "can contain only ASCII characters."

  parameter 'AvailabilityZoneLetters',
    Type: "CommaDelimitedList",
    Description: "List of availability zone suffix letters separated by a comma. Must be three entities if UseThreeAzs is true, otherwise 2. For example, 'a,b,c' or 'b,c,d'"

  parameter 'UseThreeAzs',
    Type: "String",
    Description: "If this will launch in a region that has three availability zones available, set this to 1 and specify them in AvailabilityZone:etters. Otherwise, 0.",
    MinLength: 1,
    MaxLength: 1,
    AllowedPattern: '[0-1]',
    ConstraintDescription: 'Must be binary (0 for false, 1 for true)'

  # parameter 'PrivateSubnetCidrBlocks',
  #   Type: "CommaDelimitedList",
  #   Description: "The CIDR block notation for 3 private subnets that the application hosts will potentially launch under."
  #
  # parameter 'PrivateRouteTableIds',
  #   Type: "CommaDelimitedList",
  #   Description: "The route table to associate with private subnets. This is a comma separated list of route table IDs, one per availability zone/subnet. Must be three entities if UseThreeAzs is true, otherwise 2"

  parameter 'PublicSubnetCidrBlocks',
    Type: "CommaDelimitedList",
    Description: "The CIDR block notation for 3 public subnets that the public load balancer nodes will attach to."

  parameter 'PublicRouteTableId',
    Type: "String",
    Description: "The route table ID for the route table to associate to our public subnets that load balancers are attached to.",
    MinLength: "1",
    MaxLength: "12",
    AllowedPattern: "^rtb-[0-9a-f]{8}$",
    ConstraintDescription: "Must contain 'rtb-' followed by 8 hexadecimal characters."

  # Conditions ---------------------

  condition 'ThreeAzs',
    equal(ref('UseThreeAzs'), 1)

  # Resources ----------------------

  ### Subnets

  (0..1).each do |subnetIndex|
    # public subnet setup
    subnet_name = "SubnetPublic#{subnetIndex}"

    resource subnet_name, Type: 'AWS::EC2::Subnet', Properties: {
      CidrBlock: select(subnetIndex, ref('PublicSubnetCidrBlocks')),
      AvailabilityZone: join('', ref('AWS::Region'), select(subnetIndex, ref('AvailabilityZoneLetters')) ),
      VpcId: ref('VpcId'),
      Tags: [
        { Key: 'Zone', Value: join('', ref('AWS::Region'), select(subnetIndex, ref('AvailabilityZoneLetters')) ) },
        { Key: 'Network', Value: 'Public' }
      ],
    }

    # associate the subnet to the route table
    resource "PublicSubnetRouteTableAssociation#{subnetIndex}", Type: "AWS::EC2::SubnetRouteTableAssociation", Properties: {
      SubnetId: ref(subnet_name),
      RouteTableId: ref("PublicRouteTableId"),
    }

    # # private subnet setup
    # subnet_name = "SubnetPrivate#{subnetIndex}"
    #
    # resource subnet_name, Type: 'AWS::EC2::Subnet', Properties: {
    #   CidrBlock: select(subnetIndex, ref('PrivateSubnetCidrBlocks')),
    #   AvailabilityZone: join('', ref('AWS::Region'), select(subnetIndex, ref('AvailabilityZoneLetters')) ),
    #   VpcId: ref('VpcId'),
    #   Tags: [
    #     { Key: 'Zone', Value: join('', ref('AWS::Region'), select(subnetIndex, ref('AvailabilityZoneLetters')) ) },
    #     { Key: 'Network', Value: 'Private' }
    #   ],
    # }
    #
    # # associate the route table to the subnet
    # resource "PrivateSubnetRouteTableAssociation#{subnetIndex}", Type: "AWS::EC2::SubnetRouteTableAssociation", Properties: {
    #   SubnetId: ref(subnet_name),
    #   RouteTableId: select(subnetIndex, ref('PrivateRouteTableIds'))
    # }
  end

  # Optional third subnet set

  # public subnet
  resource "SubnetPublic2", Type: 'AWS::EC2::Subnet', Condition: "ThreeAzs", Properties: {
    CidrBlock: select(2, ref('PublicSubnetCidrBlocks')),
    AvailabilityZone: join('', ref('AWS::Region'), select(2, ref('AvailabilityZoneLetters')) ),
    VpcId: ref('VpcId'),
    Tags: [
      { Key: 'Zone', Value: join('', ref('AWS::Region'), select(2, ref('AvailabilityZoneLetters')) ) },
      { Key: 'Network', Value: 'Public' }
    ],
  }

  resource "PublicSubnetRouteTableAssociation2", Type: "AWS::EC2::SubnetRouteTableAssociation", Condition: "ThreeAzs", Properties: {
    SubnetId: ref("SubnetPublic2"),
    RouteTableId: ref("PublicRouteTableId"),
  }

  # # private subnet
  # resource "SubnetPrivate2", Type: 'AWS::EC2::Subnet', Condition: "ThreeAzs", Properties: {
  #   CidrBlock: select(2, ref('PrivateSubnetCidrBlocks')),
  #   AvailabilityZone: join('', ref('AWS::Region'), select(2, ref('AvailabilityZoneLetters')) ),
  #   VpcId: ref('VpcId'),
  #   Tags: [
  #     { Key: 'Zone', Value: join('', ref('AWS::Region'), select(2, ref('AvailabilityZoneLetters')) ) },
  #     { Key: 'Network', Value: 'Private' }
  #   ],
  # }
  #
  # resource "PrivateSubnetRouteTableAssociation2", Type: "AWS::EC2::SubnetRouteTableAssociation", Condition: "ThreeAzs", Properties: {
  #   SubnetId: ref("SubnetPrivate2"),
  #   RouteTableId: select(2, ref('PrivateRouteTableIds'))
  # }

  ### Outputs ------------------------
  # requested by application stack via Custom Resource (don't repeat yourself)

  # VPC/EC2-related
  output 'VpcId', Value: ref('VpcId'), Description: "VPC ID of where the stack infrastructure resides"
  # Make comma-delimited list into a string for proper retrieval and use in sibling template
  output 'Ec2AvailabilityZone0', Value: join('', ref('AWS::Region'), select(0, ref('AvailabilityZoneLetters'))), Description: "Availability zones 1, corresponding to subnets with index 0"
  output 'Ec2AvailabilityZone1', Value: join('', ref('AWS::Region'), select(1, ref('AvailabilityZoneLetters'))), Description: "Availability zones 2, corresponding to subnets with index 1"
  # Problem: select(n, get_att("LambdaFunction", "Key")) in child stack results in 'Fn::Select requires a list argument with two elements: an integer index and a list'
  # Workaround: Since Fn::select doesn't recognize Lambda response elements as arrays or CSLs, we'll have to specify each subnet separately in the outputs
  # https://forums.aws.amazon.com/thread.jspa?messageID=626461&tstart=0#626461
  # output 'Ec2PrivateSubnetId0', Value: ref('SubnetPrivate0'), Description: "Private network subnet 0 ID"
  # output 'Ec2PrivateSubnetId1', Value: ref('SubnetPrivate1'), Description: "Private network subnet 1 ID"
  # output 'Ec2PrivateSubnetId2', Value: ref('SubnetPrivate2'), Condition: "ThreeAzs", Description: "Private network subnet 2 ID"
  output 'Ec2PublicSubnetId0', Value: ref('SubnetPublic0'), Description: "Public network subnet 0 ID for load balancing"
  output 'Ec2PublicSubnetId1', Value: ref('SubnetPublic1'), Description: "Public network subnet 1 ID for load balancing"
  output 'Ec2PublicSubnetId2', Value: ref('SubnetPublic2'), Condition: "ThreeAzs", Description: "Public network subnet 2 ID for load balancing"
  #output 'PrivateSubnetIds', Value: join(',', ref('SubnetPrivate0'), ref('SubnetPrivate1')), Description: "Private network subnet blocks"
  #output 'PublicSubnetIds', Value: join(',', ref('SubnetPublic0'), ref('SubnetPublic1')), Description: "Public network subnet blocks for load balancing"

  # Other common resources
  output 'IamSqsManagedPolicyArn', Value: ref('IamSqsManagedPolicy'), Description: "ARN of the IAM managed policy used to provide access to the app stack's SQS resource"
end.exec!
