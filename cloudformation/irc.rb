#!/usr/bin/env ruby

# Usage:
#   ruby shared_infrastructure.rb expand -Environment {ENV_NAME} > shared_infrastructure.json
#   ${EDITOR} shared_infrastructure-parameters.json
#   aws --profile {PROFILE} cloudformation create-stack --template-body file://shared_infrastructure.json --cli-input-json file://shared_infrastructure-parameters.json  --tags file://tags.json

# This script is just a sub-set of the overall infrastructure creation.
# Specifically, this sets up the application stack

require 'bundler/setup'
require 'cloudformation-ruby-dsl/cfntemplate'
require 'cloudformation-ruby-dsl/spotprice'
require 'cloudformation-ruby-dsl/table'

# Lets replace the resource DSL method with our own enhanced version
# that simply adds missing Name tags based on the resource name
class TemplateDSL < JsonObjectDSL
  # Note: we could use alias if we wanted to retain access to the old definition
  def resource(name, options)
    # not all resources support Tags... so unfortunately, we cant automatically create the Tags hash... it has to be manually specified
    if options[:Properties] and options[:Properties][:Tags]
      unless (options[:Properties][:Tags].select {|i| i['Value'] == 'Name'}).any?
        unless (name == 'AutoscalingGroup') # don't apply a name tag to the AutoScalingGroup resource.....it requires a 3rd key for 'PropagateAtLaunch'
          # by convention, add the resource name tag to all resources by adding spaces to the CamelCase resource name specified
          #options[:Properties][:Tags] << { Key: 'Name', Value: APP_NAME + ' ' + name.gsub(/([a-z])([A-Z])/, '\1 \2').strip }
          options[:Properties][:Tags] << { Key: 'Name', Value: $APP_NAME + ' ' + name.gsub(/([a-z])([A-Z])/, '\1 \2').strip }
        end
      end
    end

    default(:Resources, {})[name] = options
  end
end

# To change the prefix of most resources, interpret the -Environment switch passed to ARGV at Ruby DSL expansion of this CFN template
$APP_NAME = "Ircd"
(0..(ARGV.length - 1)).each do |argIndex|
  if ARGV[argIndex] =~ /^-/ then
    case ARGV[argIndex]
    when "-Environment"
      # override the default since value passed in for 'Environment'
      $APP_NAME = "Ircd" + ARGV[argIndex + 1].to_s
      $ENVIRONMENT = ARGV[argIndex + 1].to_s
    end
  end
end

# DSL specification of the Cloudformation JSON  ---------
template do

  value AWSTemplateFormatVersion: '2010-09-09'

  value Description: 'Creates infrastructure required to launch the resilient IRC server'

  value :Metadata => {
    :Author => 'Ian Schoonover'
  }

  parameter 'Ec2InstanceType',
    Type: "String",
    Description: "The instance type of the EC2 hosts that we are going to spin up, for example, 'm3.medium' or 'm3.large'",
    MinLength: "1",
    MaxLength: "255",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "can contain only ASCII characters."

  parameter 'KeyName',
    Type: "String",
    Description: "Existing SSH key pair name to use for ssh access to the hosts",
    Default: "gsa",
    MinLength: "1",
    MaxLength: "255",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "can contain only ASCII characters."

  parameter 'AdminSecurityGroupId',
    Type: "String",
    Description: "The security group id for the admin security group.  Used for ssh access to the application servers.",
    MinLength: "1",
    MaxLength: "11",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "can contain only ASCII characters."

  parameter 'EnvironmentName',
    Type: "String",
    Description: "Environment name where this stack resides e.g. the tag to prepend to Cloudwatch metric names",
    MinLength: "1",
    MaxLength: "20",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "Can contain only ASCII characters but should look something like a site name (Primary, Secondary, Tertiary, etc)"

  parameter 'AmiImageId',
    Type: "String",
    Description: "Base AMI for instance",
    MinLength: "12",
    MaxLength: "12",
    AllowedPattern: "^ami-[a-f0-9]{8}$",
    ConstraintDescription: "ami image id must be in the format of 'ami-xxxxxxxx'."

  parameter 'UseThreeAzs',
    Type: "String",
    Description: "If this will launch in a region that has three availability zones available, set this to 1 and specify them in AvailabilityZone:etters. Otherwise, 0.",
    MinLength: 1,
    MaxLength: 1,
    AllowedPattern: '[0-1]',
    ConstraintDescription: 'Must be binary (0 for false, 1 for true)'

  parameter 'DrActive',
    Type: "String",
    Description: "Whether the site is considered an active DR target. Only set to true in a non-primary region where the Lambda function should trigger",
    MinLength: 1,
    MaxLength: 1,
    AllowedPattern: '[0-1]',
    ConstraintDescription: 'Must be binary (0 for false, 1 for true)'

  parameter 'S3NonCurrentObjectExpireDays',
    Type: "Number",
    Description: "Number of days for the revisions of backup objects in S3 to persist",
    MinValue: 30,
    Default: 90

  parameter 'S3CurrentObjectExpireDays',
    Type: "Number",
    Description: "Number of days for the current backup objects in S3 to persist",
    MinValue: 30,
    Default: 90

  parameter 'S3BucketNamePrefix',
    Type: "String",
    Description: "Prefix to name the S3 bucket by e.g. if this were set to Ircd and the EnvironmentName to Primary, the bucket would be named Ircd-Primary",
    MinLength: "4",
    MaxLength: "20",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "Can contain only ASCII characters"

  parameter 'EmailAddress',
    Type: "String",
    Description: "Email address to use as the recipient for notifications on application health and backup success",
    MinLength: "16",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "Can contain only ASCII characters but should look like an email address"

  parameter 'ElasticIpAddress',
    Type: "String",
    Description: "Elastic IP address for the instance in this region",
    MinLength: "7",
    MaxLength: "15",
    AllowedPattern: "[\\x20-\\x7E]*",
    ConstraintDescription: "Can contain only ASCII characters but should look like an IPv4 address"

  parameter 'HostedZoneId',
    Type: "String",
    Description: "Route 53 HostedZoneId of your domain - Only required if this is the DrActive",
    # TODO: add constraint and min/max

  parameter 'Fqdn',
    Type: "String",
    Description: "Fully-qualified domain name for your application - Only required if this is the DrActive",
    # TODO: add constraint and min/max

  parameter 'WatchedRegion',
    Type: "String",
    Description: "The region ID where the application is currently running e.g. us-west-1 - Only required if this is the DrActive",
    # TODO: add constraint and min/max

  # NOTE: since cross-stack references don't work inter-region, we have to provide this as an input
  # we may be able to use the old style CustomResource method that reads outputs, but i'd rather not go back to the past
  parameter 'WatchedAutoscalingGroupName',
    Type: "String",
    Description: "The autoscaling group name in the WatchedRegion - Only required if this is the DrActive",
    # TODO: add constraint and min/max

  # Conditions -------------

  condition 'ThreeAzs',
    equal(ref('UseThreeAzs'), 1)

  condition 'DrActive',
    equal(ref('DrActive'), 1)

  # Resources --------------

  ## IAM

  # There is a IAM user that is manually created in the production account and attached to the above managed policy for webservices to access the SQS resource
  # This is done because the queue should be versioned for blue-green, but the user should always exist since webservices is not in the cloud and not blue-green

  resource "InstanceRole", Type: 'AWS::IAM::Role', Properties: {
    AssumeRolePolicyDocument: {
      Statement: [{ Effect: 'Allow', Principal: { Service: [ 'ec2.amazonaws.com' ] }, Action: [ 'sts:AssumeRole' ], }, ],
    },
    # references the managed policy granding SQS access
    ManagedPolicyArns: [ get_att('ParentStackResources', 'IamSqsManagedPolicyArn') ],
    Policies: [
      {
        PolicyName: 'InstancePolicy',
        PolicyDocument: {
          Version: "2012-10-17",
          Statement: [
            {
              Sid: "S3ListBuckets",
              Effect: "Allow",
              Action: [
                "s3:ListAllMyBuckets",
                "s3:GetBucketLocation"
              ],
              Resource: "arn:aws:s3:::*"
            },
            {
              Sid: "S3ListSpecificBucketContents",
              Effect: "Allow",
              Action: [
                "s3:ListBucket"
              ],
              Resource: interpolate("arn:aws:s3:::irc-{{$RegionDowncased}}",
              Condition: {
                StringEquals: {
                  :'s3:prefix': [
                    "",
                    "infrastructure/",
                    "releases/"
                  ],
                  :'s3:delimiter': [
                    "/"
                  ]
                }
              }
            },
            {
              Sid: "S3FullAccessToPath",
              Effect: "Allow",
              Action: "s3:*",
              Resource: interpolate("arn:aws:s3:::irc-{{$RegionDowncased}}",
              Condition: {
                StringLike: {
                  :'s3:prefix': [
                    "infrastructure/*",
                    "releases/*"
                  ]
                }
              }
            },
            {
              Sid: "SNSPublication",
              Effect: "Allow",
              Action: "sns:Publish",
              Resource: [
                "arn:aws:sns:us-west-2:183912708525:irc-notifications",
                "arn:aws:sns:us-west-2:183912708525:irc-heartbeat"
              ]
            },
            {
              Sid: "EC2AssociateAddress",
              Effect: "Allow",
              Action: [
                "ec2:AssociateAddress"
              ],
              Resource: [
                "*"
              ]
            },
            {
              Sid: "CloudWatchLogs",
              Effect: "Allow",
              Action: [
                "logs:CreateLogGroup",
                "logs:CreateLogStream",
                "logs:PutLogEvents",
                "logs:DescribeLogStreams"
              ],
              Resource: [
                "arn:aws:logs:*:*:*"
              ]
            }
          }
        }
      }
    ]
  }

  instanceProfileName = $APP_NAME + "InstanceProfile"
  resource instanceProfileName, Type: 'AWS::IAM::InstanceProfile', Properties: {
    Path: '/',
    Roles: [ ref('InstanceRole') ]
  }

  ## EC2

  # Instance Security group that allows port 80 from the load balancers
  resource 'InstanceSecurityGroup', Type: 'AWS::EC2::SecurityGroup', Properties: {
    GroupDescription: 'Allow inbound requests for http/https requests',
    VpcId: import_value(sub('${ParentStackName}-VpcId')),
    SecurityGroupIngress: [
      { IpProtocol: 'tcp', FromPort: '80', ToPort: '80', SourceSecurityGroupId: ref('PublicElbSecurityGroup') },
    ],
    SecurityGroupEgress: [
      { IpProtocol: "-1", CidrIp: "0.0.0.0/0" },
    ],
    Tags: [ { Key: "Network", Value: "Private" } ],
  }

  # TODO: download chef cookbook, craft node.json file with attribute overrides, run chef-solo
  myUserData = join('','

  ')

  # Launch Configuration setup
  resource "LaunchConfig", Type: 'AWS::AutoScaling::LaunchConfiguration', Properties: {
    ImageId: ref('AmiImageId'),
    InstanceType: ref('Ec2InstanceType'),
    KeyName: ref('KeyName'),
    IamInstanceProfile: ref('InstanceProfile'),
    InstanceMonitoring: true,
    SecurityGroups: [ ref('InstanceSecurityGroup'), ref('AdminSecurityGroupId')],
    UserData: base64(myUserData),
  }

  # Autoscaling Group setup
  resource "AutoscalingGroup", Type: 'AWS::AutoScaling::AutoScalingGroup', Properties: {
    AvailabilityZones: [
      # TODO: get resources from network stack using cross-stack outputs
      # ThreeAzs will need to be utilitzed to build this list precisely

    ],
    VPCZoneIdentifier: [
      # TODO: get resources from network stack using cross-stack outputs

    ],
    Cooldown: "360",
    DesiredCapacity: "0",
    #HealthCheckGracePeriod: ref('ElbHealthCheckGracePeriod'),
    HealthCheckType: "EC2",
    LaunchConfigurationName: ref('LaunchConfig'),
    #LoadBalancerNames: [],
    MaxSize: "0",
    MinSize: "0",
    Tags: [{
      Key: "Name",
      Value: interpolate("{{$APP_NAME}} {{ref('EnvironmentName')}}"),
      PropagateAtLaunch: true
    }]
  }

  ## Lambda

  # TODO: Add conditions that cause these resources to only be generated in a non-primary region and if the site is active.

  resource "FailoverLambdaFunction", Type: 'AWS::Lambda::Function', Properties: {
    FunctionName: interpolate("{{$APP_NAME}}Failover"),
    Code: {
      ZipFile: join("\n",
        # TODO: Paste function body here
        ""
    )},
    Handler: "index.handler",
    Runtime: "nodejs",
    Timeout: "30",
    Role: get_att("LambdaExecutionRole", "Arn"),
    Environment: {
      Variables: {
        HostedZoneId: ref('HostedZoneId'),
        Route53Fqdn: ref('Fqdn'),
        WatchedRegion: ref('WatchedRegion'),
        WatchedAutoscalingGroupName: ref('WatchedAutoscalingGroupName')
      }
    }
  }

  resource "LambdaExecutionRole", Type: "AWS::IAM::Role", Properties: {
    AssumeRolePolicyDocument: {
      Version: "2012-10-17",
      Statement: [{
        Effect: "Allow",
        Principal: { Service: [ "lambda.amazonaws.com" ] },
        Action: [ "sts:AssumeRole" ]
      }]
    },
    Path: "/",
    Policies: [{
      PolicyName: "LambdaExecutionPolicy",
      PolicyDocument: {
        Version: "2012-10-17",
        Statement: [
          {
            Sid: "CloudwatchLogsAccess",
            Effect: "Allow",
            Action: [ "logs:CreateLogGroup","logs:CreateLogStream","logs:PutLogEvents" ],
            Resource: "arn:aws:logs:*:*:*"
          },
          {
            Sid: "CloudwatchMetricsAccess",
            Effect: "Allow",
            Action: [ "cloudwatch:GetMetricStatistics", "cloudwatch:GetMetricData" ],
            Resource: "*"
          },
          {
            Sid: "AutoScalingAccess"
            Effect: "Allow",
            Action: [ "autoscaling:DescribeScalingActivities", "autoscaling:UpdateAutoScalingGroup" ],
            Resource: "*"
          },
          {
            Sid: "Route53Access"
            Effect: "Allow",
            Action: [ "route53:ChangeResourceRecordSets" ],
            Resource: "*"
          },
          {
            Sid: "S3ListAccess"
            Effect: "Allow",
            Action: [ "s3:ListBucket" ],
            Resource: "arn:aws:s3:::{{ref('S3Bucket')}}/backups"
          },
          {
            Sid: "S3DeleteAccess"
            Effect: "Allow",
            Action: [ "s3:DeleteObject" ],
            Resource: "arn:aws:s3:::{{ref('S3Bucket')}}"
            Condition: {
              StringLike: {
                :'s3:prefix': [
                  "backups/*"
                ]
              }
            }
          },
        ]
      }
    }]
  }

  ## S3

  resource "S3Bucket", Type: "AWS::S3::Bucket", Properties: {
    BucketName: interpolate("{{ref('S3BucketNamePrefix')}}-{{ref('EnvironmentName')}}"),
    # This is only useful for removing old backups.
    # Once the backup script removes the current version, old versions will be removed according to this policy
    LifecycleConfiguration: {
      Rules: [
        {
          Id: "Prune",
          NoncurrentVersionExpirationInDays: ref('S3NonCurrentObjectExpireDays'),
          ExpirationInDays: ref('S3CurrentObjectExpireDays'),
          Status: "Enabled",
        }
      ]
    },
    VersioningConfiguration: {
      Status: "Enabled"
    },
    Tags: [
      { Key: "Environment", Value: ref('EnvironmentName') }
    ],
  }

  ## SNS

  # TODO: Add Lambda subscription?

  resource "SnsTopic", Type: "AWS::SNS::Topic", Properties: {
    TopicName: interpolate("{{$APP_NAME}}Notifications"),
    Subscription: [
      {
        Endpoint: ref('EmailAddress'),
        Protocol: 'email'
      }
    ]
  }

  ## Cloudwatch

  cloudwatchAlarmName = "Cw" + $APP_NAME + "StatusChecksFailed"
  resource cloudwatchAlarmName, Type: "AWS::CloudWatch::Alarm", Properties: {
    AlarmName: "",
    AlarmDescription: "",
    Namespace: "",
    MetricName: "",
    Dimensions: [

    ],
    ComparisonOperator: "",
    Threshold: "",
    Unit: "",
    Statistic: "",
    EvaluationPeriods: "",
    Period: "",
    ActionsEnabled: 1,
    AlarmActions: [

    ],
  }

#   {
#    "Type" : "AWS::CloudWatch::Alarm",
#    "Properties" : {
#       "ActionsEnabled" : Boolean,
#       "AlarmActions" : [ String, ... ],
#       "AlarmDescription" : String,
#       "AlarmName" : String,
#       "ComparisonOperator" : String,
#       "Dimensions" : [ Dimension, ... ],
#       "EvaluateLowSampleCountPercentile" : String,
#       "EvaluationPeriods" : Integer,
#       "ExtendedStatistic" : String,
#       "InsufficientDataActions" : [ String, ... ],
#       "MetricName" : String,
#       "Namespace" : String,
#       "OKActions" : [ String, ... ],
#       "Period" : Integer,
#       "Statistic" : String,
#       "Threshold" : Double,
#       "TreatMissingData" : String,
#       "Unit" : String
#    }
# }

  # Outputs ---------------

  output 'AutoScalingGroupId', Value: ref('AutoscalingGroup')

end.exec!
