name             'irc-server'
maintainer       'ian'
maintainer_email ''
license          'GPL'
description      'Installs inspircd+anope and configures EC2 infrastructure'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.0.1'

#%w{ cbnames here }.each do |cookbook|
#  depends cookbook
#end