node.default[:services_working_path]} = "/home/ec2-user/#{node[:services_name]}-#{node[:services_version]}"

execute "Extract #{node[:services_name]} source archive" do
  command "tar xfz #{node[:chef_temp_path]}/servicessource.tgz"
  user 'ec2-user'
  group 'ec2-user'
  cwd '/home/ec2-user/'
  not_if { File.directory?("#{node[:services_working_path]}") }
end

template "Create #{node[:services_name]} expect script" do
	source "configure_#{node[:services_name]}.expect.erb"
	path "#{node[:services_working_path]}/configure_#{node[:services_name]}.expect"
	owner 'ec2-user'
	group 'ec2-user'
	mode '0550'
	action :create
end

execute "Configure make of #{node[:services_name]}" do
	command "expect configure_#{node[:services_name]}.expect"
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:services_working_path]}"
	not_if { File.directory?("#{node[:services_working_path]}/run") }
end

execute "Compile #{node[:services_name]}" do
	command 'make install'
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:services_working_path]}//build"
	not_if { File.exists?("#{node[:services_working_path]}/run/bin/services") }
end

execute "Install #{node[:services_name]}" do
	command 'make install'
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:services_working_path]}//build"
	not_if { File.exists?("#{node[:services_working_path]}/run/bin/services") }
end