package 'Install system packages from repository'
	package_name [
		'gcc', 'gcc-c++', 'cmake', 'mlocate', 
		'monit', 'tmux', 'epel-release', 'git',
		'openssl', 'openssl-devel', 'pkgconfig',
		'pcre', 'pcre-devel', 'expect', 'python-pip',
		'python26-tools', 'awslogs'
	]
	action :install
end

# this and updating /usr/bin/pip-2.7 to reference the new version might be necessary if letsencrypt client throws errors about DistributionNotFound for pip
execute 'Upgrade pip' do
  command 'easy_install --upgrade pip'
  user 'root'
  group 'root'
end

# Fix a bug in `python-virtualenv` shipped with Amazon Linux
# https://github.com/letsencrypt/letsencrypt/issues/1680#issuecomment-170641501
execute 'Upgrade python\'s virtual environment provider' do
  command 'pip install --upgrade virtualenv'
  user 'root'
  group 'root'
end

execute 'Install aws-cli via pip' do
  command 'sudo pip-2.7 install awscli'
  user 'ec2-user'
  group 'ec2-user'
end

chef_gem 'Install cookbook gem dependencies' do
	package_name 'aws-sdk'
  compile_time false
  action :install
end