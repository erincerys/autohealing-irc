set httpd port 2812
	allow 127.0.0.1

set daemon 60

template 'Create monit control file' do
	source 'monitrc.erb'
	path '/home/ec2-user/.monitrc'
	owner 'ec2-user'
	group 'ec2-user'
	mode '0700'
	action :create
end

execute 'Start monit process monitor' do
  command 'monit -l /home/ec2-user/monit.log -c /home/ec2-user/.monitrc'
  user 'ec2-user'
  group 'ec2-user'
end