node.default[:ircd_working_path]} = "/home/ec2-user/#{node[:ircd_name]}-#{node[:ircd_version]}"

execute "Extract #{node[:ircd_name]} source archive" do
  command "tar xfz #{node[:chef_temp_path]}/ircdsource.tgz"
  user 'ec2-user'
  group 'ec2-user'
  cwd '/home/ec2-user/'
  not_if { File.directory?("#{node[:ircd_working_path]}") }
end

cookbook_file "Create #{node[:ircd_name]} expect script" do
	source "configure_#{node[:ircd_name]}.expect"
	path "#{node[:ircd_working_path]}/configure_#{node[:ircd_name]}.expect"
	owner 'ec2-user'
	group 'ec2-user'
	mode '0550'
	action :create
end

execute "Configure make of #{node[:ircd_name]}" do
	command "expect configure_#{node[:ircd_name]}.expect"
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:ircd_working_path]}"
	not_if { File.directory?("#{node[:ircd_working_path]}/run") }
end

remote_file "Download #{node[:ircd_name]} extra module: m_rehashsslsignal" do
	source 'https://raw.githubusercontent.com/inspircd/inspircd-extras/master/2.0/m_rehashsslsignal.cpp'
	path "#{node[:ircd_working_path]}/src/modules/extra/m_rehashsslsignal.cpp"
	owner 'ec2-user'
	group 'ec2-user'
	mode '0554'
	action :create
end

link "Symlink #{node[:ircd_name]} extra module: m_rehashsslsignal" do
	to "#{node[:ircd_working_path]}/src/modules/m_rehashsslsignal.cpp"
	target_file "#{node[:ircd_working_path]}/src/modules/extra/m_rehashsslsignal.cpp"
	owner 'ec2-user'
	group 'ec2-user'
	link_type :symbolic
	action :create
end

execute "Compile #{node[:ircd_name]}" do
	command 'make'
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:ircd_working_path]}"
	not_if { File.exists?("#{node[:ircd_working_path]}/run/build/bin/inspircd") }
end

execute "Install #{node[:ircd_name]}" do
	command 'make INSTUID=ec2-user install'
	user 'ec2-user'
	group 'ec2-user'
	cwd "#{node[:ircd_working_path]}"
	not_if { File.exists?("#{node[:ircd_working_path]}/run/bin/#{node[:ircd_name]}") }
end