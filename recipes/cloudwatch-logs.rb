directory '/opt/aws/cloudwatch' do
  owner 'root'
  group 'root'
  mode '0755'
  recursive true
end

template "/etc/awslogs/awscli.conf" do
  source 'cwlogs.cfg.erb'
  owner 'root'
  group 'root'
  mode '0644'
end

execute 'Schedule AWS CloudWatch Logs agent' do
  command { 'chkconfig awslogs on'}
  user 'root'
  not_if { system 'chkconfig --list | grep -c awslogs' == 1 }
end
 
execute 'Start AWS CloudWatch Logs agent' do
  command { 'service awslogs start' }
  user 'root'
  not_if { system 'pgrep -f aws-logs-agent-setup" }
end