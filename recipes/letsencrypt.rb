remote_file 'Download LetsEncrypt client' do
	source 'https://github.com/letsencrypt/letsencrypt/releases/tag/v0.4.2'
	path "#{node[:chef_temp_path]}/letsencryptclient.tgz"
	owner 'ec2-user'
	group 'ec2-user'
	mode '750'
	action :create
end

execute "Extract LetsEncrypt client archive" do
  command "tar xfz #{node[:chef_temp_path]}/letsencryptclient.tgz"
  user 'ec2-user'
  group 'ec2-user'
  cwd '/home/ec2-user/'
  not_if { File.directory?("/home/ec2-user/letsencrypt") }
end

execute 'Install LetsEncrypt dependencies' do
  command '/home/ec2-user/letsencrypt/letsencrypt-auto --debug --os-packages-only'
  user 'ec2-user'
  group 'ec2-user'
end

ruby_block "Download LetsEncrypt configuration archive" do
  block do
    require 'aws-sdk'

    Aws.config[:ssl_ca_bundle] = "${node[:chef_temp_path]}/curl-ca-bundle.crt"
    s3_client = Aws::S3::Client.new(region: "#{node[node[:environment]][:region]}")
    s3_client.get_object(bucket: "#{node[node[:environment]][:bucket]}",
        key: 'certificates/letsencryptconfig-current.tar.gz',
        response_target: "#{node[:chef_temp_path]}/letsencryptconfig.tar.gz")
  end
  not_if { File.exist?("${node[:chef_temp_path]}\letsencryptconfig.tar.gz") }
  action :run
end

directory 'Create LetsEncrypt configuration directory' do
	path '/etc/letsencrypt'
	owner 'root'
	group 'root'
	mode '0755'
	action :create
end

execute "Extract LetsEncrypt configuration archive" do
  command "tar xfz #{node[:chef_temp_path]}/letsencryptconfig.tar.gz"
  user 'root'
  group 'root'
  cwd '/etc/letsencrypt'
  not_if { File.directory?('/etc/letsencrypt/archive') }
end

template 'Create LetsEncrypt configuration file' do
	source 'letsencrypt-config.ini.erb'
	path '/etc/letsencrypt/cli.ini'
	owner 'root'
	group 'root'
	mode '0740'
	action :create
end

template 'Create LetsEncrypt renewal script' do
  path '/home/ec2-user/letsencrypt/certificate-renewal.sh'
  source 'certificate-renewal.sh.erb'
  owner 'ec2-user'
  group 'ec2-user'
  mode '0750'
  variables {
  	{
  		letsencrypt_cert_path: "/etc/letsencrypt/live/#{node[:primary_fqdn]}",
  		ircd_working_path: "#{node[:ircd_working_path]}",
			sns_notification_arn: "#{node[:sns_notification_arn]}",
			ircd_name: "#{{node[:ircd_name]}}",
			primary_fqdn: "#{node[:primary_fqdn]}"
  	}
  }
end

cron 'Schedule certificate renewal' do
	minute '0'
	hour '1' # 6pm PT
	user 'ec2-user'
  #environment {
  # 'AWS_CONFIG_FILE' => '/home/ec2-user/.aws/config'
  # 'AWS_SHARED_CREDENTIALS_FILE' => '/home/ec2-user/.aws/credentials'
  #} # don't need this with IAM role
  path '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/aws/bin:/home/ec2-user/bin'
  shell '/bin/bash'
  # TODO: shove in /var/log and place under control of logrotate
	command '/home/ec2-user/letsencrypt/certificate-renewal.sh >> /home/ec2-user/letsencrypt/run.log 2>&1'
end

file "Copy #{node[:ircd_name]} SSL certificate chain" do
  content ::File.open("/etc/letsencrypt/live/#{node[:primary_fqdn]}/fullchain.pem")
  path "#{node[:ircd_working_path]}/conf/server.crt"
  user 'ec2-user'
  group 'ec2-user'
  mode '0750'
  action :create
end

file "Copy #{node[:ircd_name]} SSL certificate key" do
  content ::File.open("/etc/letsencrypt/live/#{node[:primary_fqdn]}/privkey.pem")
  path "#{node[:ircd_working_path]}/conf/server.key"
  user 'ec2-user'
  group 'ec2-user'
  mode '0750'
  action :create
end
