template 'Create backup script' do
	source 'backup.sh.erb'
	path '/home/ec2-user/backup.sh'
	owner 'ec2-user'
	group 'ec2-user'
	mode '0750'
	action :create
end

cron 'Schedule backup script' do
	minute '0' # every hour
	user 'ec2-user'
	#environment {
	#	'AWS_CONFIG_FILE' => '/home/ec2-user/.aws/config'
	#	'AWS_SHARED_CREDENTIALS_FILE' => '/home/ec2-user/.aws/credentials'
	#} # don't need this with IAM role
	path '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/aws/bin:/home/ec2-user/bin'
	shell '/bin/bash'
	command '/home/ec2-user/backup.sh'
end