# TODO
# intent is
# - to download a newer version of the ircd or services
# - reinstall (recompile, reconfigure, etc)
# probably most of the recipes already written can be reinvoked

include_recipe 'download-artifacts'

### ircd compilation ###

include_recipe 'compile-ircd'

### irc services compilation ###

include_recipe 'compile-services'

## TODO: need configuration of ircd and services blocks here

# stop ircd
execute "Stop #{node[:ircd_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc stop #{node[:ircd_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]}" }
end

# stop services
execute "Stop #{node[:services_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc stop #{node[:services_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]} && pgrep #{node[:services_name]}" }
end

### Process monitor config uppdate ###

include_recipe 'monit'

# start ircd
execute "Stop #{node[:ircd_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc start #{node[:ircd_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]}" }
end

# start services
execute "Start #{node[:services_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc start #{node[:services_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]} && pgrep #{node[:services_name]}" }
end
