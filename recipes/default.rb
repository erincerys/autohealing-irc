### Prepare system ###

cookbook_file 'Write bash rc configuration' do
  source '.bashrc'
  path '/home/ec2-user/.bashrc'
  owner 'ec2-user'
  group 'ec2-user'
  mode '644'
  action :create
end

cookbook_file 'Write bash profile configuration' do
  source '.bash_profile'
  path '/home/ec2-user/.bash_profile'
  owner 'ec2-user'
  group 'ec2-user'
  mode '644'
  action :create
end

directory 'Create Chef run working directory' do
	path '/tmp/chef'
	action :create
end

include_recipe 'dependencies'

template 'Create system update and notification script' do
	path '/home/ec2-user/update-system.sh'
	source 'update-system.sh.erb'
	owner 'ec2-user'
	group 'ec2-user'
	mode '0750'
end

cron 'Schedule system update and notification script' do
	minute '0'
	hour '0' # 5pm PT
	user 'ec2-user'
	#environment {
	#	'AWS_CONFIG_FILE' => '/home/ec2-user/.aws/config'
	#	'AWS_SHARED_CREDENTIALS_FILE' => '/home/ec2-user/.aws/credentials'
	#} # don't need this with IAM role
	shell '/bin/bash'
	path '/usr/local/bin:/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/sbin:/opt/aws/bin:/home/ec2-user/bin'
	command '/home/ec2-user/update-system.sh'
end

### check if kernel update is available ###

#
# schedule checking pkg mgmt for new kernel available
# send sns message to notify of pending update and forwarn of reboot
# write recipe for updating system, including kernel and rebooting
#

### configure openssh ###

#
# template openssh server configuration
#

### Lifecycle hook - pause

#
# call hold hook
#

### Download artifacts ###

# TODO: check in on this
include_recipe 'download-artifacts'

### ircd compilation ###

#include_recipe 'compile-ircd'

### irc services compilation ###

#include_recipe 'compile-services'

### ircd binary retrieval ###

# TODO: write recipe to download precompiled ircd binaries, and extract into place
include_recipe ''

### services binary retrieval ###

# TODO: write recipe to download precompiled services binaries , and extract into place
include_recipe ''

### LetsEncrypt ###

include_recipe 'letsencrypt'

### ircd configuration ###

#
# template blocks for each config file
# copy letsencrypt certificates into place
## allows for env specific config in cfn/attribute overrides
# break this out into its own recipe for use in updates
# write ec2 run command templates to perform updates?
#

template 'Create ircd configuration file: settings.conf' do
	path "#{node[:ircd_working_path]}/conf/settings.conf"
	source 'settings.conf.erb'
	owner 'ec2-user'
	group 'ec2-user'
	mode '0750'
	variables lazy {
		nameserver: `grep nameserver /etc/resolv.conf | awk '{ print $2 }'`
	}
end

### irc services configuration ###

#
# template blocks for each config file
## allows for env specific config in cfn/attribute overrides
# break this out into its own recipe for use in updates
# write ec2 run command templates to perform updates?
#

### Backup job ###

include_recipe 'schedule-backups'

### Process monitor ###

include_recipe 'monit'

### Log shipping ###

include_recipe 'cloudwatch-logs'

### Start services ###

#
# write a resource/provider to interact with monit
# or look for one already written
#

execute "Start #{node[:ircd_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc start #{node[:ircd_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]}" }
end

execute "Start #{node[:services_name]} application" do
  command "monit -c /etc/ec2-user/.monitrc start #{node[:services_name]}"
  user 'ec2-user'
  group 'ec2-user'
  not_if { system "pgrep #{node[:ircd_name]} && pgrep #{node[:services_name]}" }
end

### Healthcheck ###

#
# nginx install, light config, start
# irkerd install and start
# irk hook script to send message
# write a cgi script to
# - read irkerd log to find if connection successful
# - return proper http code
#

### Alter Chef run list in user data and perhaps reboot ###

#
# if kernel update was done,
# alter rc.local to give different run list specifying recipe to resume lifycycle
# write that recipe to then modify rc.local again to only do system updates
# reboot
# if no kernel update,
# alter rc.local to give different run list specifying recipe that only does system updates
# resume lifecycle
#
