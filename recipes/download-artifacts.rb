remote_file 'Download certificate authority bundle' do
	path "${node[:chef_temp_path]}/curl-ca-bundle.crt"
	source 'https://curl.haxx.se/ca/cacert.pem'
	action :create
end

remote_file "Download #{node[:ircd_name]} source archive" do
	source "https://github.com/inspircd/inspircd/archive/v#{node[:ircd_version]}.tar.gz"
	path "#{node[:chef_temp_path]}/ircdsource.tgz"
	checksum 'ba428d4d176b1b59b6302f76b5de8de8042f0defc3b8cbeb6a8e7a9652c2f67f'
	owner 'ec2-user'
	group 'ec2-user'
	mode '750'
	action :create
end

remote_file "Download #{node[:services_name]} source archive" do
	source "https://github.com/anope/anope/archive/#{node[:services_version]}.tar.gz"
	path "#{node[:chef_temp_path]}/servicessource.tgz"
	checksum '15e041bf3ebe0c86002e41162e98beb9276a910ef43628c46b58ae0a230401e2'
	owner 'ec2-user'
	group 'ec2-user'
	mode '750'
	action :create
end
